
import './App.css';
import React, {useState} from 'react';
import MainHeader from './components/MainHeader/MainHeader';
import Login from './components/Login/Login';
import Home from './components/Home/Home';
//import MainHeader from './components/MainHeader';

function App() {

  const [isLoggedIn,setIsLoggedIn] = useState(false);
  const loginhandler = (email,password) => {
      setIsLoggedIn(true);
  }
  const logouthandler = () => {
    setIsLoggedIn(false);
  }

  return (
    <React.Fragment>
      <MainHeader  isAuthenticated={isLoggedIn} onLogout={logouthandler}/>
     <main>
      {!isLoggedIn && <Login  onLogin = {loginhandler} />}
      {isLoggedIn && <Home onLogout={logouthandler} />} 
     </main>
    
    </React.Fragment>
  );
}

export default App;
