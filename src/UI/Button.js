import classes from './Button.module.css';
import React from 'react';

const Button = (props) =>
{
    return(
        <button
            type = {props.type || 'submit'}
            className = {`${props.className} ${classes.button}`}
            disabled = {props.disabled}
            onClick={props.onClick}
        >{props.children}</button>
    );
}
export default Button;