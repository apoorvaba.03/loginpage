import Card from '../../UI/Card';
import Button from '../../UI/Button'
import React, { useState } from 'react';

import classes from './Login.module.css';

const Login = (props) => {
   
    const [enteredEmail,setEmail] = useState('');
    const [emailIsValid,setEmailIsValid] = useState();

    const [enteredPassword,setPassword] = useState('');
    const [passwordIsValid,setPasswordIsValid] = useState('');

    const [formIsValid, setFormIsValid] = useState(false);

// submit handler
const submitHandler = (event) => {
    event.preventDefault();

    //console.log(enteredEmail,enteredPassword);
    props.onLogin(enteredEmail,enteredPassword);
};

// email store
const emailChangeHandler = (event) => {
    setEmail(event.target.value);

    setFormIsValid(
        event.target.value.includes('@') && enteredPassword.trim().length >6
    );
    console.log(formIsValid);
    
}
// password store
const passwordChangeHandler = (event) =>{
    setPassword(event.target.value);
    setFormIsValid(
        event.target.value.trim().length > 6 && enteredEmail.includes('@')
      );
      console.log(formIsValid);
}
// emailblur

const validateEmailHandler = () => {
    setEmailIsValid(enteredEmail.includes('@'));
};

//passwordbllur

const validatePasswordHandler = () => {
    setPasswordIsValid(enteredPassword.trim().length >6);
}

    return(
        <Card className={classes.login}>

            <form onSubmit={submitHandler}> 
                <div className={`${classes.control}
                ${emailIsValid === false ? classes.invalid : ''}`}>
                    <label htmlFor="email"> E-mail</label>
                    <input id="email"
                     type="email" 
                        onChange={emailChangeHandler} 
                        value={enteredEmail}
                        onBlur={validateEmailHandler}
                      />
                </div>
                <div className={`${classes.control} 
                ${passwordIsValid === false ? classes.invalid : ''}`}>
                    <label htmlFor="password"> Password</label>
                    <input id="password"
                     type="password" 
                     onChange={passwordChangeHandler} 
                     value={enteredPassword}
                     onBlur={validatePasswordHandler}
                      />
                </div>
                <div>
                    <Button type="submit" className={classes.btn} disabled={!formIsValid}>Login</Button>
                </div>

            </form>
        </Card>
    );
}
export default Login;